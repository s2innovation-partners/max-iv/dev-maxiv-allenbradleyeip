"""Contains the tests for the AllenBradleyEIP device server."""

# Imports
import time
from mock import MagicMock, call

import PyTango
from unittest import skip
from devicetest import DeviceTestCase

import sys
print sys.path

import pyeip
import allenbradleyeip.device


def get_tag_info(tag):
    tag_type_map = {"tag_A": pyeip.T_CIP_DINT,
                    "tag_B": pyeip.T_CIP_BOOL,
                    "tag_C": pyeip.T_CIP_REAL,
                    "tag_D": pyeip.T_CIP_SINT}
                    # "tag_G": pyeip.T_CIP_SINT}
    if tag == "tag_G":
        raise pyeip.TagMissingError
    return {"tag": tag, "type": tag_type_map[tag]}


# Device test case
class AllenBradleyEIPTestCase(DeviceTestCase):

    device = allenbradleyeip.device.AllenBradleyEIP
    properties = {
        "PLC": "testplc",
        "CPUSlot": 4,
        "MinimumScanPeriod": 0.01,
        "Tags": [
            "tag_A",
            "tag_B, 1.3",
            "tag_C, 4.2, attr_C",
            "tag_D, -3",
            "tag_E, hello",
            "#tag_F",
            "tag_G"
        ]
    }

    @classmethod
    def mocking(cls):
        # time
        cls.time = allenbradleyeip.device.time = allenbradleyeip.scanlist.time = MagicMock()
        cls.time.time.return_value = time.time()
        def fake_sleep(dt):
            start_time = cls.time.time()
            time.sleep(0.01)
            cls.time.time.return_value = start_time + dt
        cls.time.sleep.side_effect = fake_sleep

        # PyEIP
        cls.pyeip = allenbradleyeip.device.pyeip = MagicMock()

        # methods
        cls.connection = MagicMock()
        cls.connection.connect.return_value = True
        cls.pyeip.Eip.return_value = cls.connection
        cls.get_tag_info = get_tag_info
        cls.connection.get_tag_info.side_effect = get_tag_info

        # exceptions
        cls.pyeip.EIPError = pyeip.EIPError
        cls.pyeip.TagMissingError = pyeip.TagMissingError
        cls.pyeip.ScanlistError = pyeip.ScanListError
        cls.pyeip.PLCConnectionError = pyeip.PLCConnectionError

    def tearDown(self):
        # This is needed, or we interfere with other tests in the same run
        allenbradleyeip.scanlist.time = time

    # def test_plc_is_completed(self):
    #     self.connection.complete.assert_called_once_with(self.device.name())

    def test_state_normally_running(self):
        self.assertEqual(self.device.State(), PyTango.DevState.RUNNING)

    # def test_state_fault_if_not_connected(self):
    #     #self.connection.connect.side_effect = pyeip.PLCConnectionError
    #     self.device.Init()
    #     self.assertEqual(self.device.State(), PyTango.DevState.FAULT)

    def test_scanlists_scanned_on_init(self):
        scans = self.connection.scan.call_args_list
        for period in [0, 1.3, 4.2, -3]:
            self.assertIn(call(period), scans)

    def test_tag_attributes_correctly_added(self):
        attrs = self.device.get_attribute_list()
        self.assertIn("tag_A", attrs)
        self.assertIn("tag_B", attrs)
        self.assertNotIn("tag_C", attrs)
        self.assertIn("attr_C", attrs)
        self.assertIn("tag_D", attrs)

    def test_misconfigured_tags_left_unparsed(self):
        unparsed_tags = self.device.GetUnparsedTags()
        self.assertEqual(["tag_E, hello"], unparsed_tags)

    def test_commented_tags_ignored(self):
        attrs = self.device.get_attribute_list()
        self.assertNotIn("tag_F", attrs)

    def test_tag_attributes_correctly_typed(self):
        for attr, atype in [("tag_A", PyTango.DevLong),
                            ("tag_B", PyTango.DevBoolean),
                            ("attr_C", PyTango.DevFloat),
                            ("tag_D", PyTango.DevShort)]:
            info = self.device.get_attribute_config(attr)
            self.assertEqual(info.data_type, atype)

    def test_tag_attributes_correct_periods(self):
        for tag, period in [("tag_A", 0),
                            ("tag_B", 1.3),
                            ("tag_C", 4.2),
                            ("tag_D", -3)]:
            dperiod = self.device.GetPeriodForTag(tag)
            self.assertEqual(dperiod, period)

    # skip("""this test is failing because initialize_dynamic_attributes
    #         is apparently only run the first time Init is called.""")
    def test_missing_tag_on_plc_(self):
        # def _get_tag_info(plc, tag):
        #     if tag == "tag_G":
        #         raise pyeip.TagMissingError
        #     return get_tag_info(plc, tag)
        # self.connection.get_tag_info.side_effect = _get_tag_info
        # self.device.Init()
        self.assertIn("tag_G", self.device.GetBadTags())
        self.assertNotIn("tag_G", self.device.get_attribute_list())

    def test_unpolled_tag_scanned_on_read(self):
        self.time.time.return_value += 1.1  # pass time since init scan
        self.connection.scan.reset_mock()
        self.device.read_attribute("tag_A")
        self.connection.scan.assert_called_with(0)

    def test_unpolled_tag_not_scanned_if_read_quickly(self):
        self.connection.scan.reset_mock()
        self.time.time.return_value += 0.001  # less than min scan time
        self.device.read_attribute("tag_A")
        self.assertFalse(self.connection.scan.called)

    def test_writing_several_unpolled_tags_in_same_scanlist(self):
        self.time.time.return_value += 10
        self.connection.scan.reset_mock()
        self.device.read_attributes(["tag_A", "tag_G"])
        self.connection.scan.assert_called_once_with(0)

    def test_attribute_valid_when_poll_thread_on_time(self):
        self.time.time.return_value += 1
        result = self.device.read_attribute("tag_B")
        self.assertEqual(result.quality, PyTango.AttrQuality.ATTR_VALID)

    def test_attribute_invalid_when_poll_thread_late(self):
        self.time.time.return_value += 10
        result = self.device.read_attribute("tag_B")
        self.assertEqual(result.quality, PyTango.AttrQuality.ATTR_INVALID)

    # def test_state_goes_to_fault_on_scan_error(self):
    #     self.time.time.return_value += 1.1  # pass time since init scan
    #     self.connection.scan.reset_mock()
    #     self.connection.scan.side_effect = pyeip.PLCConnectionError
    #     self.device.read_attribute("tag_A")
    #     self.assertEqual(self.device.State(), PyTango.DevState.FAULT)


# Main execution
if __name__ == "__main__":
    import unittest
    unittest.main()
