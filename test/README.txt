The tests currently interfere with each other. Easiest way to get them running
is to run each file individually, e.g:

$ nosetests test_allenbradleyeip.py
$ nosetests test_thread.py
$ nosetests test_scanlist.py 

