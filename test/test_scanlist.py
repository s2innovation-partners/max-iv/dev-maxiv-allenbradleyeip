from functools import partial
from mock import Mock, MagicMock, patch
from threading import Lock
import time
from unittest import TestCase

from allenbradleyeip.scanlist import ScanList


class ScanListTestCase(TestCase):

    def setUp(self):
        self.eip = Mock()
        self.lock = MagicMock()
        self.logger = Mock()
        self.scanlist = ScanList(self.eip, self.lock, self.logger,
                                 period=1.23, minimum_period=1.0)

    def test_scan_not_performed_too_often(self):
        self.scanlist.last_scanned = time.time() - 0.1
        self.assertFalse(self.scanlist.scan())


# Main execution
if __name__ == "__main__":
    import unittest
    unittest.main()
