from functools import partial
from mock import Mock, patch
import time
from unittest import TestCase

from allenbradleyeip.pollthread import PeriodicScanlistPollThread


def fake_sleep(fake_time, dt):
    fake_time.return_value += dt


class PeriodicScanlistPollThreadTestCase(TestCase):

    def setUp(self):
        self.logger = Mock()
        self.scanlist = Mock()
        self.scanlist.period = 1.4
        self.scanlist.tags = set(["A", "B", "C"])
        self.callback = Mock()
        self.errback = Mock()
        self.thread = PeriodicScanlistPollThread(self.logger, self.scanlist, self.callback, self.errback)
        self.sleep = time.sleep

    def tearDown(self):
        self.thread.running = False

    def test_runs_scans(self):
        with patch("time.time") as mock_time, patch("time.sleep") as mock_sleep:
            mock_time.return_value = 1
            mock_sleep.side_effect = partial(fake_sleep, mock_time)
            self.thread.start()
            self.sleep(0.01)
        self.assertTrue(self.scanlist.scan.called)

    def test_slow_scans_reported(self):
        with patch("time.time") as mock_time, \
             patch("time.sleep") as mock_sleep, \
             patch("random.random") as mock_random:
            mock_time.return_value = 1
            mock_sleep.side_effect = partial(fake_sleep, mock_time)
            mock_random.return_value = 123
            self.scanlist.scan.side_effect = lambda: fake_sleep(mock_time, 10)
            self.thread.start()
            self.sleep(0.01)
        # there should be no sleeping if the scanning is slow
        # (apart from the initial random sleep)
        mock_sleep.assert_called_once_with(123)
        self.assertTrue(self.logger.warn.called)

    def test_failed_scans_cause_backoff(self):
        with patch("time.time") as mock_time, patch("time.sleep") as mock_sleep:
            mock_time.return_value = 1
            mock_sleep.side_effect = partial(fake_sleep, mock_time)
            self.scanlist.scan.side_effect = Exception
            self.thread.start()
            self.sleep(0.01)
        # check that we step through up to max backoff
        for t in range(self.thread.max_backoff, self.thread.backoff):
            mock_sleep.assert_called_with(t)
        self.assertEqual(mock_sleep.call_args_list[-1][0],
                         (self.thread.max_backoff,))
        self.assertTrue(self.logger.error.called)


# Main execution
if __name__ == "__main__":
    import unittest
    unittest.main()
