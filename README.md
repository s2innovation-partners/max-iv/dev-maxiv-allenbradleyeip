# AllenBradleyEIP

This is a Tango device that communicated with a Rockwell Allen-Bradley
PLC and exposes a set of tags on the PLC as attributes. Writing and reading
is supported. It can poll tags at different periods and can send out
change events.

It uses the "etherip" C library from SNS to talk to the PLC.

The PLC exports values as *tags*, where each tag has a symbolic name
and a value which can be of various types (various INT sizes, REAL,
BOOL...) or an array of such values.

In order to read and write tags, the library (and therefore the
device) is structured around the concept of a *scanlist*. This is a
set of tags that are read and written as a unit, e.g. in one request
to the PLC. When a scanlist is scanned, all tags that have changed in
the PLC are read out and all tags that have changed in the device are
written to the PLC.

In principle it's possible to create one scanlist per tag, but due to
how the PLC communication protocol works that's very
inefficient. Instead, the tags should be grouped in scanlists after
how often they need to be updated (see below for more details).

In the library, each scanlist is labeled according to its "period",
i.e.  how often it is to be read/written. This means all scanlists
need to have a period, even those that don't need periodic scanning, or
"polling", e.g. tags that are only ever written, and infrequently
so. To enable this, the convention used in the device is to give
non-polled scanlists zero or negative period. This way they can still
be grouped into different scanlists if needed.

Polling scanlists also enables the use of change events.

The device represents each tag as an attribute with the appropriate
Tango type. One thing to be aware of is that the device has no way to
know if a tag is writable on the PLC without actually trying to write
it. It therefore represents all tag attributes as writable. The device
will raise an error if write is attempted on a tag that is not
writable.  Currently writing to a polled non-writable tag may put the
device in a non-recoverable state requiring restart.


## Properties

### PLC

The IP or hostname of the PLC.

### Tags

A list of strings each describing a PLC tag to be accessed. The tag must
obviously exist on the PLC. The string is composed of up to three parts,
separated by commas (","). Only the first part is not optional.

- Name. Must be the same as in the PLC, including case.

- Polling period [s]. If not specified, defaults to 0. If the period
  is 0 or less, the tag will not be polled internally by the device -
  i.e. no change events - but it can still be read and written. All
  tags with the same period (whether polled or not) belong to the same
  scanlist.

- Attribute name. If not specified, the tag name is used.

#### Examples

    tag1            # not polled
    tag1,0          # same thing
    tag1,-1         # not polled, but part of another scanlist
    tag2,5          # polled every 5 s
    tag3,1,attr1    # polled every 1 s, attribute named "attr1"
    tag4[5],0,tag4  # element 5 of an array tag
                    # (you must provide an attribute name in this case)

## Attributes

The device dynamically creates one attribute per configured tag. The
type of the tag is read from the PLC and used to infer a suitable Tango
type.

Note: All attributes are both readable and writable even though the tags
may not be. This is because there is currently no way to obtain this info
from the PLC. Trying to write a non-writable tag will result in an error.


## Commands


### GetUnparsedTags

Returns a list of tags that could not be successfully parsed from the
"Tags" property. This should only happen if the configuration deviates
from the format described above.


### GetBadTags

Returns a list of tags that could not be found on the PLC during
startup. These tags will not have corresponding attributes
created. The reason for this is usually that the tag name is
incorrect.


### GetFilteredTags

Takes a string argument which should be a filter (e.g. a substring or
a regex) which will be matched against all the tags on the device. The
return value is a list of strings containing the names and values of
any matching tags.


## Events

Change events are supported for polled tag attributes, but for that
they must be configured with absolute or relative deltas in the
database, since the device uses the Tango event filter. This does not
apply to State or boolean attributes, as they will always send change
events if they are polled.


## Error handling

Handling is currently pretty crude. The device enters FAULT state if
there are any errors, and there's no automatic reconnect. Usually this
requires a restart of the server.


## Notes on configuration

- Consider the number of tags in a scanlist. Technically I have not seen
  any limit to the size, but the number and type of the tags (and the
  length of the tag names) determine the polling/reading time, since it's
  very dependent on the request size. The total time to read a scanlist
  scales pretty much linearly. Typically the time is around 1 ms per tag.
  This all puts a hard limit on the minimum polling period possible.

- Since all scanlist threads share a connection, they may be fighting
  over access. Therefore it's good to split tags that need fast updating and
  slower ones into different scanlists. Don't poll tags more often than
  needed.

- It is not recommended to use a polling period that is shorter than
  the average time it takes to run a scan. It can make the poll times
  unpredictable and also cause errors (see troubelshooting section
  below). Also there isn't really any point. You may have to tune the
  polling periods a bit by trial and error. The Status command can be
  used to get measurements of the poll times and actual scan periods.

- Tags that don't need polling (e.g. those that are rarely read and/or
  written) can be put in 0 or negative periods. They can still be
  split up into different scanlists, if needed, to improve read/write
  times. But note that writing a tag also means that the corresponding
  scanlist must be scanned, which may mean first waiting for polled
  scans to finish. If you rely on attribute writes not taking too long
  to return, consider scanning your writable tags too. They will then
  be written to the PLC on the next scan.

- Another difference between polled and unpolled tags is that a
  read/write operation on an attribute corresponding to an unpolled
  tag will take as long as it takes for the device to perform a poll
  on the affected scanlist, while operations on polled tags will
  return instantly as they operate only on the in-memory scanlist
  buffer.

- Running several devices in one server but connected to the same PLC
  means that the devices will share a single connection. Scanlists
  from all devices on the server will be competing for scan time.

- There is apparently a performance gain (up to ~50%) to be had from
  running two separate connections (i.e. servers) to one PLC but more
  than that does not seem to improve performance. OTOH it does not
  seem to hurt significantly either. This might differ between
  different PLC models though.

- For large deployments you'll have to find a compromise between the polling
  intervals, the number of tags per scanlist/device, and servers.

- Also keep in mind that there is a maximum number of concurrent
  connections to a single PLC, but it's probably above 20.

- The "System Overhead Time Slice" setting in the PLC can be increased for
  a possible improvement in response times. Some quick tests have revealed
  speedups of a factor between 2 and 3. This may impact the performance
  of the PLC itself in some cases.


## Troubleshooting

### General performance issues

Keep an eye on the output of the Status command. If a scanlist's
"average period" is longer than its configured period (if positive),
the polling is taking too long. The reason may be that there are
simply too many tags to be read at the configured rate. Remember that
*all* devices talking to the same PLC will compete for bandwidth.

Also check the "average period" of all unpolled scanlists (i.e. those
with poll period <=0). If it is short (i.e. around the period of your
polled scanlists) it means that someone is periodically reading tags in
that list. This can be very inefficient and in that case there is
really no reason to no put those tags in a polled scanlist.  As a
rule, all tags that are read periodically should be polled from the
PLC, especially since they can then send out events. 


### Read errors/INVALID values

If there are problems with reading attributes from the device, it is
likely that the polling period set for those tags is too short,
i.e. shorter than the time it typically takes to perform a scan. This
will mean that the tags are not updated at the expected rate and at
some point the device will report the tag values as INVALID.

Solution: increase the polling period until the scan time is always
shorter than the period. Also see the configuration chapter above
for some tips on optimization.


### Device in FAULT state

If your device never gets beyond the FAULT state, the first thing to
check is the Status. If it reports that it cannot connect to the PLC,
or that there are no valid tags, it is a configuration issue. 

If a previously working device goes into FAULT, this is likely caused
by a restart or reset of the PLC, or a network issue. Currently there
is no mechanism to automatically recover from this case and the device
will go into FAULT state since it cannot read any scanlists. It's
possible that the device can recover by itself if the connection
somehow stays alive.

Solution: try Init() but if that does not help, the server needs to be
restarted.


## Future developments

- Better array support: there is a lot of performance to gain from bunching
  tag values into arrays, if we can read them all at once.

- More robust error handling, reconnect etc.

- Allow tags to be configured as read-only?

- Support for reading standardised function blocks without having to
  configure all the tags?
