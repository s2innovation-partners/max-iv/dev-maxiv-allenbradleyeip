import os
import json
import time
from copy import deepcopy
from string import Template
import random
from subprocess import Popen

import PyTango
from dsconfig.configure import configure
from dsconfig.filtering import filter_config
from dsconfig.formatting import SERVERS_LEVELS, decode_dict

# A random string to be used for uniquifying names
PREFIX = "".join(random.sample("ABCDEFGHIJKLMNOPQRTTUVWXYZ1234567890", 10))


def add_prefix(config, prefix):
    # Add the given prefix to all instance and device names
    modified = deepcopy(config)  # cheating!
    srvs = modified["servers"]
    for name, srv in list(srvs.items()):
        srvname, instname = name.split("/")
        srvs[srvname + "/" + prefix + "-" + instname] = srvs.pop(name)
        for clsname, clss in srv.items():
            for devname in list(clss.keys()):
                clss[prefix + "-" + devname] = clss.pop(devname)
    return modified


class Device(object):

    """This fixture provides a single device running in its
    own server process. The device can be accessed through the
    'proxy' attribute."""

    def __init__(self, request, device, config, path="", retries=50):

        self.processes = {}
        self.config = {}

        # the finalizer is run after all tests are done
        request.addfinalizer(self.teardown)

        # substitute any template variables in the file, e.g. $PREFIX
        tmp = Template(config).substitute(PREFIX=PREFIX)

        config = json.loads(tmp, object_hook=decode_dict)

        # get the part of the config that concerns us, ignore the rest
        filtered = {"servers": filter_config(
            config["servers"], ["device:%s" % device],
            SERVERS_LEVELS)}

        if not filtered["servers"]:
            raise ValueError("Failed to find configuration for device %s!"
                             % device)

        # By adding a random prefix to servers and devices in the
        # config, we can be reasonably sure to not collide with existing stuff.
        self.config = add_prefix(filtered, PREFIX)

        # Here we write to the database
        configure(self.config, write=True, update=True)

        # Run our device in the background
        # (Note: There should really only ever be one server...)
        for item in self.config["servers"].keys():
            servername, instname = item.split("/")
            self.processes[item] = (
                Popen([os.path.join(path, servername), instname, "-v3"]))

        # create proxy for device
        self.proxy = PyTango.DeviceProxy(PREFIX + "-" + device)
        for i in xrange(retries):  # Let's wait for it to come alive
            try:
                self.proxy.ping()
                break
            except PyTango.DevFailed:
                time.sleep(0.2)
        else:
            raise(RuntimeError("Device %s did not start up!" % device))

    def teardown(self):

        # stop the server
        for proc in self.processes.values():
            proc.terminate()
            proc.wait()

        # finally, clean up in the database
        for server in self.config.get("servers", []):
            PyTango.Database().delete_server(server)
