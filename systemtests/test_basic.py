"""
These tests run an AllenBradleyEIP device in a real Tango environment, talking
to a simulated PLC (but the device thinks it's real). A server will be created
in the DB but it should get automatically cleaned up after the tests.
The server and device have "uniquified" names with a random string prefix to
prevent name collisions.

Requirements:

* a non-production Tango database (stuff will be written to it)
* the "cpppo" python package (installable from PyPi) for simulating a PLC
* the "dsconfig" python module for configuring the DB
* the AllenBradleyEIP module in your PYTHONPATH and the start script to be in
  your $PATH.

"""

from itertools import izip, tee
import json
import random
from subprocess import Popen
import time
from threading import Event

from cpppo.server.enip import client
import PyTango
import pytest

from fixture import Device


# # #  CONFIGURATION  # # #

PLC = "localhost"

# Generate a bunch of tag names
p1 = [-2, -1, 0]
p2 = [.1, .2, .3]
int_tags = [("INT_%d" % i, random.choice(p1)) for i in xrange(10)]
polled_int_tags = [("INT_POLLED_%d" % i, random.choice(p2))
                   for i in xrange(10)]
real_tags = [("REAL_%d" % i, random.choice(p1)) for i in xrange(10)]
polled_real_tags = [("REAL_POLLED_%d" % i, random.choice(p2))
                    for i in xrange(10)]
misc_tags = [(t, random.choice(p1)) for t in "ABCDEF"]

print polled_int_tags + polled_real_tags

# give each tag a randomly chosen scanlist
tag_property = (["%s,%.1f" % tag
                 for tag in int_tags + real_tags + misc_tags] +
                ["BAD1", "BAD2"] +
                ["%s,%.1f" % tag
                 for tag in polled_int_tags + polled_real_tags] +
                ["89,WRONG"])


CONFIG = json.dumps({
    "servers": {
        "AllenBradleyEIP/1": {
            "AllenBradleyEIP": {
                "test/eip/1": {
                    "properties": {
                        "PLC": [PLC],
                        "Tags": tag_property,
                        "MinimumScanPeriod": 0
                    },
                    "attribute_properties": dict(
                        (tagname, {"abs_change": ["1"]})
                        for tagname, period in (polled_int_tags +
                                                polled_real_tags))
                }
            }
        }
    }
})


# # #  FIXTURES # # #

@pytest.yield_fixture(scope="module")
def plc(request):

    "Fake PLC fixture"

    int_tag_args = ["%s=INT" % tag[0] for tag in int_tags + polled_int_tags]
    real_tag_args = ["%s=REAL" % tag[0] for tag in real_tags +
                     polled_real_tags]
    misc_tag_args = ["%s=INT" % tag[0] for tag in misc_tags]

    process = Popen(["python", "-m", "cpppo.server.enip"] +
                    int_tag_args + real_tag_args + misc_tag_args)

    # make sure the server is up before yielding
    for x in xrange(50):
        try:
            conn = client.connector(host=PLC, timeout=5)
            break
        except:
            time.sleep(0.2)
    else:
        pytest.xfail("Could not connect to fake PLC")

    yield conn  # the connection can be used to read/write tags

    # --> TESTS RUN HERE <--

    # teardown
    process.terminate()
    process.wait()


@pytest.fixture()
def allenbradleyeip(request):

    "AllenBradleyEIP device fixture"

    return Device(request, "test/eip/1", CONFIG)


# # #  Helpers  # # #

def write_tag(plc, tag, value, tol=1e-5):
    "write a tag directly to the simulated PLC"
    with plc:
        result = plc.synchronous(
            operations=client.parse_operations(["%s=%r" % (tag, value)]))
        for r in result:
            plc.validate(r)
    # wait until the read is completed
    while abs(read_tag(plc, tag) - value) > tol:
        time.sleep(0.1)


def read_tag(plc, tag):
    "read a tag directly from the simulated PLC"
    with plc:
        result = plc.synchronous(
            operations=client.parse_operations(["%s" % tag]))
        value = list(result)[-1][-1][0]
    return value


def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)


# # #  TESTS  # # #

def test_device_is_running(plc, allenbradleyeip):
    assert allenbradleyeip.proxy.State() == PyTango.DevState.RUNNING


def test_device_reports_bad_tags(plc, allenbradleyeip):
    assert set(allenbradleyeip.proxy.GetBadTags()) == set(["BAD1", "BAD2"])


def test_device_reports_unparsed_tags(plc, allenbradleyeip):
    assert allenbradleyeip.proxy.GetUnparsedTags() == ["89,WRONG"]


def test_device_reports_tags_in_scanlist(plc, allenbradleyeip):
    result = allenbradleyeip.proxy.GetTagsInScanlist(-1)
    scanlist = [tagname
                for tagname, period in int_tags + real_tags + misc_tags
                if period == -1]
    assert set(result) == set(scanlist)


def test_device_reports_tag_period(plc, allenbradleyeip):
    for tag, period in (int_tags + real_tags + polled_int_tags +
                        polled_real_tags + misc_tags):
        assert allenbradleyeip.proxy.GetPeriodForTag(tag) == period


def test_device_read_unpolled_tag(plc, allenbradleyeip):
    TAG = "INT_6"
    VALUE = random.randint(-100, 100)
    write_tag(plc, TAG, VALUE)
    assert allenbradleyeip.proxy.read_attribute(TAG).value == VALUE


def test_device_read_polled_tag(plc, allenbradleyeip):
    TAG = "INT_POLLED_6"
    PERIOD = dict(polled_int_tags)[TAG]
    print "period", PERIOD
    VALUE = random.randint(-100, 100)
    write_tag(plc, TAG, VALUE)
    print "readback", read_tag(plc, TAG)

    time.sleep(PERIOD*10)  # make sure to allow one polling period
    assert allenbradleyeip.proxy.read_attribute(TAG).value == VALUE


# TODO: Writing polled and unpolled tags


def test_device_sends_events_for_polled_tags(plc, allenbradleyeip):
    """The device should automatically send out change events for polled tags that
    have event configuration."""
    x = 13
    TAG = "INT_POLLED_%d" % 2
    done = Event()

    def listener(event):
        # only fire the event when the expected event arrives
        if event.attr_value.value == x:
            done.set()

    allenbradleyeip.proxy.subscribe_event(
        TAG, PyTango.EventType.CHANGE_EVENT, listener)

    write_tag(plc, TAG, x)

    assert done.wait(timeout=5)  # wait for the callback to fire


# def test_tango_filters_events_by_attr_config(plc, allenbradleyeip):

#     """This test is slow and a bit flaky, but it's not really
#     testing the device itself, but the Tango event filtering
#     functionality."""

#     TAG = "INT_POLLED_%d" % 2
#     got_event = Event()

#     def listener(event):
#         print "event", event.attr_value
#         got_event.set()

#     allenbradleyeip.proxy.subscribe_event(
#         TAG, PyTango.EventType.CHANGE_EVENT, listener)

#     time.sleep(3)  # give time for initial events to pass

#     for last, new in pairwise([0, 2, 3, 10, 11, 14, 20, 10]):
#         print last, new
#         got_event.clear()
#         write_tag(plc, TAG, new)
#         if abs(new - last) > 5:
#             assert got_event.wait(timeout=3)
#         else:
#             assert not got_event.wait(timeout=3)
