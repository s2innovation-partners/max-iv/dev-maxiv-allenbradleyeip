#!/usr/bin/env python

from setuptools import setup

setup(name = "tangods-allenbradleyeip",
      version = "1.0.2",
      author = "Johan Forsberg",
      author_email = "johan.forsberg@maxlab.lu.se",
      license = "GPLv3",
      url = "http://www.maxlab.lu.se",
      description = "Device server for Allen-Bradley PLC communication.",
      packages = ['allenbradleyeip'],
      test_suite = "nose.collector",
      scripts = ['scripts/AllenBradleyEIP']
)
