import pyeip
import PyTango


def eip_type_to_tango(eip_type):
    "convert from EtherIP type code to PyTango type"
    # TODO: check these!
    return {
        pyeip.T_CIP_BOOL: PyTango.CmdArgType.DevBoolean,
        pyeip.T_CIP_SINT:  PyTango.CmdArgType.DevShort,  # 8 bit integer
                                             # or DevUChar? Is it signed?
        pyeip.T_CIP_INT: PyTango.CmdArgType.DevShort,    # 16 bit integer
        pyeip.T_CIP_DINT: PyTango.CmdArgType.DevLong,    # 32 bit integer
        pyeip.T_CIP_REAL: PyTango.CmdArgType.DevFloat    # 32 bit float
    }.get(eip_type)


def eip_value_to_python(info, value):
    "So far, only booleans need special treatment"
    if info["type"] == pyeip.T_CIP_BOOL:
        return bool(value)
    return value


def python_value_to_eip(info, value):
    "ditto"
    if info["type"] == pyeip.T_CIP_BOOL:
        return [0, 255][value]
    return value
