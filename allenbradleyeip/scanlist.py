from collections import deque
from itertools import tee, izip
import time

import numpy as np

from pyeip import PLCConnectionError, ScanListError
from convert import eip_value_to_python, python_value_to_eip


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)


class ScanList(object):

    """Keeps track of a scanlist. A scanlist is the internal EIP representation of a set
    of tags that are to be polled as one unit, i.e. with the same period."""

    def __init__(self, eip, global_lock, logger,
                 period, minimum_period=None):
        self.eip = eip
        self.lock = global_lock
        self.logger = logger
        self.period = period     # the polling period in s. May be 0
                                 # or less; means it won't be polled

        # don't scan more often than this
        self.minimum_period = minimum_period or period / 2
        self.tags = set()        # names of all the tags in the list
        self.last_scanned = 0    # last time we did a scan
        self.needs_scan = False  # whether we've got changes to write

        self.errors = 0
        self.scan_times = deque(maxlen=10)
        self.scan_durations = deque(maxlen=10)
        self.tag_info = {}

        self.is_polled = self.period > 0  # this never changes

    def get_info(self):
        info = self.eip.get_scanlist_info(self.period)
        return info

    def add_tag(self, tagname):
        "add a tag to the scanlist"
        self.eip.add_tag(tagname, self.period, 1)
        self.tags.add(tagname)

    @property
    def average_scan_time(self):
        "the measured scan time averaged over the last ten scans"
        if len(self.scan_durations) == 10:
            return np.average(list(self.scan_durations))
        return -1.

    @property
    def average_scan_period(self):
        """the measured scan period (i.e. time between two consecutive scans)
        averaged over the last ten scans"""
        if len(self.scan_times) == 10:
            return np.average([t2 - t1
                               for t1, t2 in pairwise(self.scan_times)])
        return -1.

    def is_slow(self, amount=.2):
        "Return whether the scanlist is polling slower than expected"
        if self.period <= 0:
            return False  # a non-polled scanlist can't be slow
        return self.average_scan_period > self.period * (1 + amount)

    def remove_tag(self, tagname):
        # I haven't seen a way to remove a tag from a scanlist once
        # it's been added, probably we'd have to recreate the scanlist
        # from scratch. However, scanning a list with broken tags
        # seems to be OK, they just can't be read. Sure it's less
        # efficient, but you really shoundn't have broken tags in the
        # config anyway...
        self.tags.remove(tagname)

    def get_tag_info(self, tagname):
        "Return the tag info."
        if tagname in self.tag_info:
            return self.tag_info[tagname]
        # tag info does not change, so let's cache it.
        info = self.tag_info[tagname] = self.eip.get_tag_info(tagname)
        return info

    def get_tag(self, tagname):
        """Get the last read value for a tag."""
        # with self.lock:
        value = self.eip.get_tag(tagname)
        info = self.eip.get_tag_info(tagname)
        if info["elements"] == 1:
            return eip_value_to_python(info, value[0])
        else:
            return [eip_value_to_python(info, element) for element in value]

    def set_tag(self, tagname, value):

        """Set a value for a tag. The value will be written to the PLC
        the next time the scanlist is run."""

        info = self.get_tag_info(tagname)
        # TODO: do we need a lock here? I think etherip can handle changes
        # during a scan, but I'm unsure if the threading can mess things up...
        with self.lock:
            self.eip.set_tag(tagname, python_value_to_eip(info, value))
        self.needs_scan = True

    def get_tag_values(self):
        "an iterator that yields tag and current value for all tags"
        for tagname in self.tags:
            value = self.eip.get_tag(tagname)[0]
            converted = eip_value_to_python(self.get_tag_info(tagname), value)
            yield tagname, converted

    def scan(self):
        t0 = time.time()
        if not self.needs_scan and self.last_scanned > (t0 - self.minimum_period):
            # limit the scanning frequency
            return False
        self.scan_times.append(t0)
        try:
            with self.lock:
                self.eip.scan(self.period)
            self.last_scanned = t0
            self.scan_durations.append(time.time()-t0)
        except (PLCConnectionError, ScanListError) as e:
            self.logger.error("Scan error in scanlist %f: %s" % (self.period, e))
            self.errors += 1
            raise
        self.needs_scan = False
        return True
