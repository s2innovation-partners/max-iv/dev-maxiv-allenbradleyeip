"Allen-Bradley PLC communication Tango device"

import json
import time
import re
from itertools import chain
from operator import attrgetter
from threading import Lock
from random import random

import block
import pyeip
from convert import eip_type_to_tango
from pollthread import PeriodicScanlistPollThread
from scanlist import ScanList
from utils import CaselessDict

import PyTango
from PyTango.server import Device, DeviceMeta, command, device_property, run


# The buffer limit sets the maximum size of one single request to the PLC
# in bytes. Low numbers will impact the performance negatively as each scanlist
# needs to be split up into more separate requests. Also note that this is
# also the upper limit of the size readable for a single tag. Large arrays
# cannot be read as they (currently) cannot be split over requests.
# This is a magic number for our PLCs; the default of 500 is too large and
# causes problems with large scanlists. Be very careful about making it bigger.
# If there are problems with creating scanlists, try decreasing it.
# (TODO: make it a property)
ETHERIP_BUFFER_LIMIT = 494


class AllenBradleyEIP(Device):

    """A Tango device that connects to an Allen-Bradley PLC via EtherIP,
    and monitors a set of tags. Both reading and writing is
    supported. Each tag will get a corresponding attribute of a
    matching type. Tango change events are sent *if configured*.
    """

    __metaclass__ = DeviceMeta

    PLC = device_property(dtype=str, doc="hostname of the PLC.")
    CPUSlot = device_property(dtype=int, doc="CPU slot on the PLC",
                              default_value=0)
    MinimumScanPeriod = device_property(dtype=float,
                                        doc="Don't scan more often than this.",
                                        default_value=1.0)
    Tags = device_property(
        dtype=[str], doc="List of tagnames, polling periods, attribute names",
        default_value=[])

    Blocks = device_property(
        dtype=[str], doc="List of function block name, blocktype",
        default_value=[])

    UseEventFilter = device_property(
        dtype=bool, doc="Whether to use the TANGO change event filter",
        default_value=True)

    def init_device(self, *args, **kwargs):

        self.get_device_properties()

        self.threads = {}
        self.lock = Lock()  # for synchronizing communication between threads

        self.scanlists = {}
        self.block_scanlists = {}

        self.tag_config = {}
        self._tag_status = "Tags not initialized."

        if not self.PLC:
            self.fatal_stream("No PLC configured!")
            return

        # Each device has to use an unique name for the PLC, do differentiate
        # the scanlists from other devices running in the same server.
        # The device name is an obvious choice.
        # Note: Maybe it's actually a good idea to share some scanlists across
        # devices? It could have some unexpected consequences, though.
        self.plcname = self.get_name()

        # the Eip object is where all the data is really kept;
        # scanlists are created and tags added. When a "scan" is run,
        # an internal struct is updated with the latest values from
        # the PLC, and any values that have written to it are sent to
        # the PLC. The methods available to set and get tags are
        # merely writing to this struct - without scanning, nothing is
        # communicated to the PLC.

        # Only supports one PLC per device for now. It would make the config
        # more complex as each tag would have to belong to a specific PLC.
        # It seems more straightforward to use one (or more) device per PLC.

        try:
            self.eip = pyeip.Eip(self.PLC, slot=self.CPUSlot, verbosity=1,
                                 buffer_limit=ETHERIP_BUFFER_LIMIT,
                                 plcname=self.plcname)
        except pyeip.PLCConnectionError as e:
            self.fatal_stream(str(e))
            self.set_state(PyTango.DevState.FAULT)
            self.eip = None
            return

        self.debug_stream("Connected to PLC at {0}.".format(self.PLC))

        # mappings between tagnames and attribute names
        self._tag_attr_map = CaselessDict()
        self._attr_tag_map = CaselessDict()

        # parse the tag configuration
        (self.tag_config,
         skipped_tags, self._unparsed_tags) = self.parse_tags()
        self.blocks, skipped_blocks, self._unparsed_blocks = self.parse_blocks()

        self.debug_stream("Parsed %d tags and %d blocks, skipped: %d, failed: %d" %
                          (len(self.tag_config), len(self.blocks),
                           len(skipped_tags),
                           len(self._unparsed_tags)))

        if not (self.tag_config or self.blocks):
            self.fatal_stream("No tags configured!")
            self._tag_status = "No tags configured."
            self.set_state(PyTango.DevState.FAULT)
            return
        if self._unparsed_tags:
            self.warn_stream("Could not parse the following tags: %r"
                             % self._unparsed_tags)

        # create scanlists and add tags
        self._create_scanlists()
        self.debug_stream("Done creating scanlists.")

        # contact the PLC to fill in all the info (e.g. type,
        # size...) for all tags. This must be done before we can
        # read or write anything.
        try:
            self.eip.complete()
        except pyeip.TagMissingError:
            self.fatal_stream("Could not find any of the configured tags!")
            self._tag_status = "None of the configured tags were found on the PLC!"
            self.set_state(PyTango.DevState.FAULT)
            return
        self.debug_stream("PLC info completed.")

        # do a first scan and setup polling
        for scanlist in self.scanlists.values():
            self.debug_stream("Polling scanlist {0} ({1} tags)"
                              .format(scanlist.period, len(scanlist.tags)))
            try:
                scanlist.scan()
            except (pyeip.PLCConnectionError, pyeip.TagMissingError,
                    pyeip.ScanListError) as e:
                self.fatal_stream("Error setting up scanlist: %r" % e)
                self.set_state(PyTango.DevState.FAULT)
                return  # fatal error; cannot start operating
        self.debug_stream("Scanlists initialized.")

        for scanlist in self.block_scanlists.values():
            self.debug_stream("Polling block scanlist {0} ({1} tags)"
                              .format(scanlist.period, len(scanlist.tags)))
            try:
                scanlist.scan()
                # TODO: looks like this always succeeds; even if the block does
                # not exist..?
            except (pyeip.PLCConnectionError, pyeip.TagMissingError,
                    pyeip.ScanListError) as e:
                self.error_stream("Error setting up block scanlist: %r" % e)

        # get tag info and weed out unreadable tags
        self._bad_tags = self.check_tag_info()
        self._bad_blocks = self.check_block_info()

        self._tag_status = ("{0} tags configured ({1} bad)."
                            .format(len(self.tag_config), len(self._bad_tags)))

        self.start_polling()

    @PyTango.DebugIt()
    def delete_device(self):
        super(AllenBradleyEIP, self).delete_device()
        self.stop_polling()
        self.info_stream("Polling stopped.")
        if self.threads:
            time.sleep(1.0)  # Some time to let ongoing scans finish...
        # with self.lock:  # ...and make sure they are done.
        #     if self.eip:
        #         self.eip.shutdown()
        #         self.info_stream("PLC communication shut down.")

    def _thread_failures(self):
        return sum(thread.failures > 0 for thread in self.threads.values())

    def _all_scanlists_complete(self):
        return all(scanlist.last_scanned for scanlist in self.scanlists.values())

    def dev_state(self):
        state = self.get_state()
        if state == PyTango.DevState.FAULT:
            # This means the config is broken or something else prevents
            # us from starting correctly. Always return FAULT.
            return state
        if self._thread_failures():
            # FAULT if polling is (partially) broken
            return PyTango.DevState.FAULT
        if (any(scanlist.is_slow() for scanlist in self.scanlists.values())):
            # go into ALARM if some scanlists are slow
            return PyTango.DevState.ALARM
        return state

    def dev_status(self):
        self._status = self._make_status()  # need to keep a reference or it
                                            # will be GC:d too soon :P
        return self._status

    def _make_status(self):
        now = time.time()
        scanlist_status = ""

        for scanlist in sorted(self.scanlists.values(),
                               key=attrgetter("period")):
            avg = scanlist.average_scan_time
            between = scanlist.average_scan_period
            scanlist_status += (
                (" * Scanlist {0}: {1} tags; last scan {2:.2} s ago\n"
                 "   average period {3:.4} s; "
                 "{4:.4} s/scan; {5} errors\n")
                .format(scanlist.period, len(scanlist.tags),
                        now - scanlist.last_scanned,
                        between, avg, scanlist.errors))
        if not self.PLC:
            plc_status = "No PLC configured!"
        elif not self.eip:
            plc_status = "Could not connect to PLC '%s'!" % self.PLC
        else:
            plc_status = "PLC: %s" % self.PLC
        thread_failures = self._thread_failures()
        if thread_failures:
            thread_status = ("{0} threads are failing to scan."
                             .format(thread_failures))
        elif len(self.threads):  # TODO: this doesn't look right...
            thread_status = ("All {0} scanlist threads OK."
                             .format(len(self.threads)))
        else:
            thread_status = "There are no scanlist threads."

        status_str = "\n".join(["Device is in %s state" % self.dev_state(),
                                plc_status, self._tag_status, thread_status,
                                scanlist_status])
        return status_str

    def _create_scanlists(self):
        self.debug_stream("_create_scanlists")
        self.scanlists = {}
        for tagname, config in self.tag_config.items():
            period = config["period"]
            if period not in self.scanlists:
                scanlist = ScanList(self.eip, self.lock,
                                    self.get_logger(), period,
                                    self.MinimumScanPeriod)
                self.scanlists[period] = scanlist
                self.debug_stream("Created scanlist with period {0}"
                                  .format(period))
            self.scanlists[period].add_tag(tagname)
        for blockname, config in self.blocks.items():
            period = config["period"]
            blocktype = config["type"]
            scanlist = ScanList(self.eip, self.lock, self.get_logger(),
                                period, self.MinimumScanPeriod)
            self.debug_stream("Created scanlist for block '%s', period %f" % (blockname, period))
            self.block_scanlists[blockname] = scanlist
            for tag in getattr(block, blocktype):
                scanlist.add_tag("%s.%s" % (blockname, tag))

    @PyTango.DebugIt()
    def _get_scanlist_for_tag(self, tagname):
        config = self.tag_config[tagname]
        period = config["period"]
        return self.scanlists[period]

    def write_attr_hardware(self, attr_indices):
        # if self.get_state() == PyTango.DevState.FAULT:
        #     PyTango.Except.throw_exception("COMMUNICATION_BROKEN", "PLC connection error",
        #                                    "", PyTango.ErrSeverity.ERR)
        # Note: this will be pretty inefficient if many tags belonging to
        # the same scanlist were to be written consecutively, since the
        # scanlist will be scanned once per tag. The solution is to write
        # all tags in one call.
        attrs = self.get_device_attr()
        attributes = [attrs.get_attr_by_ind(i) for i in attr_indices]
        to_scan = set()
        for attr in attributes:
            tagname = self._attr_tag_map[attr.get_assoc_name()]
            scanlist = self._get_scanlist_for_tag(tagname)
            scanlist.set_tag(tagname, attr.get_write_value())
            if not scanlist.is_polled:
                # If the tag is polled, it will get written the next scan,
                # if not, we must write it now.
                to_scan.add(scanlist)
        for scanlist in to_scan:
            try:
                scanlist.scan()
            except pyeip.EIPError as e:
                PyTango.Except.throw_exception("COMMUNICATION_BROKEN",
                                               "Error scanning PLC",
                                               str(e), PyTango.ErrSeverity.ERR)

    def read_attr_hardware(self, attr_indices):
        attrs = self.get_device_attr()
        attributes = [attrs.get_attr_by_ind(i) for i in attr_indices]
        to_scan = set()
        for attr in attributes:
            tagname = self._attr_tag_map[attr.get_assoc_name()]
            scanlist = self._get_scanlist_for_tag(tagname)
            if not scanlist.is_polled:
                # unpolled scanlists must be scanned before reading
                to_scan.add(scanlist)
        for scanlist in to_scan:
            try:
                scanlist.scan()
            except pyeip.EIPError as e:
                PyTango.Except.throw_exception("COMMUNICATION_BROKEN",
                                               "Error scanning PLC",
                                               str(e), PyTango.ErrSeverity.ERR)

    @PyTango.DebugIt()
    def read_tag(self, attr):
        """General method for reading a tag attribute"""
        attrname = attr.get_name()
        tagname = self._attr_tag_map[attrname]
        scanlist = self._get_scanlist_for_tag(tagname)
        value = scanlist.get_tag(tagname)
        if (scanlist.is_polled and
            (self.threads[scanlist.period].failures
             or scanlist.last_scanned + 5 * scanlist.period < time.time())):
            # This is a bit loose; if too much time has passed since the last
            # scan, we'll consider the value invalid. Is 5*period too long?
            quality = PyTango.AttrQuality.ATTR_INVALID
        else:
            quality = PyTango.AttrQuality.ATTR_VALID
        attr.set_value_date_quality(value, int(scanlist.last_scanned), quality)

    @PyTango.DebugIt()
    def write_tag(self, attr):
        """General method for writing a tag attribute"""
        # Note: it's all done in 'write_attr_hardware' now

    def is_tag_allowed(self, tag):
        return self.get_state() == PyTango.DevState.RUNNING

    def parse_blocks(self):
        blocks = {}
        skipped = set()
        failed = set()
        for i, config in enumerate(self.Blocks):
            if config.startswith("#"):
                skipped.append(config)
                continue
            try:
                blockname, blocktype = config.split(",")
                period = -1000 - random()  # fixme
                blocks[blockname] = {"period": period, "type": blocktype}
            except ValueError:
                self.warn_stream("Could not parse Blocks property, line %d: '%s'."
                                 % (i, config))
                failed.append(config)

        return blocks, skipped, failed

    @PyTango.DebugIt()
    def parse_tags(self):
        """Read the Tags property and figure out the config from there.
        Format is '<tagname>[, <polling period in s>[, <attribute name>]]'
        """
        tags = {}

        skipped = list()
        failed = list()
        for i, config in enumerate(self.Tags):
            if config.startswith("#"):
                # skip commented out tags
                skipped.append(config)
                continue
            # first figure out the tag configuration
            try:
                parts = config.split(",")
                tagname = parts[0].strip()
                if len(parts) == 1:
                    period = 0
                else:
                    period = float(parts[1].strip())
                if len(parts) > 2:
                    attrname = parts[2].strip()
                else:
                    attrname = tagname
            except ValueError:
                self.warn_stream("Could not parse Tags property, line %d: '%s'."
                                 % (i, config))
                failed.append(config)
                continue
            tags[tagname] = {"period": period, "attrname": attrname}
        return tags, skipped, failed

    @PyTango.DebugIt()
    def check_tag_info(self):

        """Retrieve tag info, containing e.g. the data type. This is also
        used as a check that the tag exists in the scanlist."""

        bad_tags = set()

        def remove_tag(tagname):
            bad_tags.add(tagname)
            self.tag_config.pop(tagname)
            self.scanlists[config["period"]].remove_tag(tagname)
            try:  # remove attribute if it exists
                self.remove_attribute(config.get("attrname", tagname))
            except PyTango.DevFailed:
                pass

        for tagname, config in self.tag_config.items():
            try:
                info = self.eip.get_tag_info(tagname)
                config["tag_info"] = info
                attrname = config.get("attrname", tagname)
                tango_type = eip_type_to_tango(info["type"])
                # Note: currently all tags of unsupported types are
                # ignored. This particularly affects the "struct" type
                # which is used for function blocks that can contain
                # other tags or blocks. For now the whole name of all
                # tags need to be specified, e.g. "FB_A.subblock_B.tag_C"
                if not tango_type:
                    self.warn_stream("Tag '{0}' has unsupported type ({1})."
                                     .format(tagname, info["type"]))
                    remove_tag(tagname)
                    continue
                self._tag_attr_map[tagname] = attrname
                self._attr_tag_map[attrname] = tagname
            except pyeip.TagMissingError:
                self.warn_stream("Tag '{0}' does not exist on plc {1}."
                                 .format(tagname, self.PLC))
                remove_tag(tagname)

        if bad_tags:
            self.warn_stream(
                "The following tags could not be read from the PLC: %r" %
                bad_tags)
        return bad_tags

    def check_block_info(self):

        """Verify that all configured function blocks exist and that allValues
        the expected sub-tags are available (according to block type)"""

        bad_blocks = set()
        for blockname, config in self.blocks.items():
            try:
                subtags = getattr(block, config["type"])
                # check that all tags in the function block exist
                for subtag in subtags:
                    # We're not interested in the info, just that it can be read
                    self.eip.get_tag_info("%s.%s" % (blockname, subtag))
            except pyeip.TagMissingError:
                self.debug_stream("Could not read %s.%s, so skipping block" % (blockname, subtag))
                self.blocks.pop(blockname)
                self.block_scanlists.pop(blockname)
                bad_blocks.add(blockname)
        if bad_blocks:
            self.warn_stream(
                "The following function blocks could not be read from the PLC: %s" %
                ",".join(bad_blocks))
        return bad_blocks

    @PyTango.DebugIt()
    def initialize_dynamic_attributes(self):
        """Setup an attribute per tag."""

        # Note that this method gets run after *all* devices in the server
        # have gone through "init_device()". So the attributes won't exist
        # until then, meaning that some devices may start polling before
        # there are any attributes to be read...
        if self.get_state() == PyTango.DevState.FAULT:
            # We're broken, no point in proceeding
            return

        # Turning off Tango serialization.
        # This is (apparently) needed in order for several devices to
        # work reliably in the same server. Otherwise there may be
        # issues with failing to acquire serialization. I *think* this
        # is safe anyway. But really, find out a better way.

        util = PyTango.Util.instance()
        util.set_serial_model(PyTango.SerialModel.NO_SYNC)

        for tagname, config in self.tag_config.items():
            info = config["tag_info"]
            tango_type = eip_type_to_tango(info["type"])
            attrname = config.get("attrname", tagname)
            attr = PyTango.Attr(attrname, tango_type, PyTango.READ_WRITE)
            self.add_attribute(attr, r_meth=self.read_tag,
                               w_meth=self.write_tag,
                               is_allo_meth=self.is_tag_allowed)
            # handle events for polled tags, and let Tango detect changes
            self.set_change_event(attrname, config["period"] > 0,
                                  self.UseEventFilter)

        # ...and we turn it on again :)
        util = PyTango.Util.instance()
        util.set_serial_model(PyTango.SerialModel.BY_DEVICE)

        self.set_state(PyTango.DevState.RUNNING)

    def send_events_for_scanlist(self, scanlist):
        "Send out change events for the tags in a scanlist"

        # Note: since we're using the "detect" functionality of the
        # attribute event system, we push all events every time and
        # let Tango sort out which should really be sent out to
        # listeners depending on the attribute event configs.
        # Note that this also means that no events get sent out unless
        # the attribute is configured for it!

        # TODO: this is a bit naive; we're assuming that if any
        # scanlist is OK, we're fine.
        self.set_state(PyTango.DevState.RUNNING)

        for tagname, value in scanlist.get_tag_values():
            attr = self._tag_attr_map[tagname]
            try:
                self.push_change_event(attr, value)
            except PyTango.DevFailed as e:
                # TODO: This is a workaround for the fact that we
                # don't actually know if the attributes have been
                # created yet.
                self.warn_stream("Could not push change event for tag '%s': %r"
                                 % (tagname, e))

    def handle_polling_error(self, period):
        # TODO: some more sophisticated error handling
        # self.set_state(PyTango.DevState.FAULT)
        pass

    @PyTango.DebugIt()
    def start_polling(self):
        """Create and start all polling threads."""
        for period, scanlist in self.scanlists.items():
            if scanlist.is_polled:
                thread = PeriodicScanlistPollThread(
                    self.get_logger(), scanlist, self.send_events_for_scanlist,
                    self.handle_polling_error)
                self.threads[period] = thread
                thread.start()

    @PyTango.DebugIt()
    def stop_polling(self):
        for period, thread in self.threads.items():
            del self.threads[period]
            thread.stop()
            thread.join()  # I think it's OK to not join, as the
                           # threads are just sleeping and will then
                           # exit. Otherwise there is an issue with
                           # Tango timeouts.

    @command(dtype_in=str, dtype_out=str)
    def ReadFunctionBlock(self, blockname):
        """Read a "function block" from the PLC, by name.
        Returns a JSON encoded string with all the tags and their values."""
        scanlist = self.block_scanlists.get(blockname)
        if scanlist:
            scanlist.scan()
            return json.dumps(dict(scanlist.get_tag_values()))
        raise ValueError(blockname)  # TODO: raise Tango exception

    @command(dtype_out=[str])
    def ListFunctionBlocks(self):
        return sorted("%s (%s)" % (block, config["type"]) for block, config in self.blocks.items())

    @command(dtype_out=[str])
    def GetBadTags(self):
        return sorted(self._bad_tags)

    @command(dtype_out=[str])
    def GetBadBlocks(self):
        return sorted(self._bad_blocks)

    @command(dtype_out=[str])
    def GetUnparsedTags(self):
        return sorted(self._unparsed_tags)

    @command(dtype_out=[str])
    def GetUnparsedBlocks(self):
        return sorted(self._unparsed_blocks)

    @command(dtype_in=float, dtype_out=[str])
    def GetTagsInScanlist(self, period):
        scanlist = self.scanlists[period]
        return sorted(scanlist.tags)

    @command(dtype_in=str, dtype_out=float)
    def GetPeriodForTag(self, tagname):
        scanlist = self._get_scanlist_for_tag(tagname)
        return scanlist.period

    @command(dtype_in=str, dtype_out=[str])
    def GetFilteredTags(self, pattern):
        """Return a list of strings containing all tags matching the given
        regular expression, and their latest values."""
        regex = re.compile(pattern, re.IGNORECASE)
        all_tags = chain(*[scanlist.tags
                           for scanlist in self.scanlists.values()])
        filtered = (tag for tag in all_tags if regex.search(tag))
        result = sorted("%s=%r" % (tag, self.eip.get_tag(tag))
                        for tag in filtered)
        return result

    # @command(dtype_in=float, dtype_out=str)
    # def GetScanlistInfo(self, period):
    #     info = self.eip.get_scanlist_info(period)
    #     result = "Info for scanlist with period %f" % info["period"]
    #     for item in info.items():
    #         result += "\n * %s: %r" % item
    #     return result

    @command(dtype_in=str, dtype_out=str)
    def GetTagInfo(self, tagname):
        info = self.eip.get_tag_info(tagname)
        result = "Info for tag '%s':" % info["tag"]
        for item in info.items():
            result += "\n * %s: %r" % item
        return result


def main():
    run((AllenBradleyEIP,))


if __name__ == "__main__":
    main()
