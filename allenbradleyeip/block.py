# Digital alarm block
ALMD = [
    "AlarmCountReset",
    "InAlarm",
    "Acked",
    "InAlarmUnack",
    "Disabled",
    "AlarmCount",
    "InAlarmTime",
    "AckTime",
    "RetToNormalTime",
    "AlarmCountResetTime"
]

# Analog alarm block
ALMA = [
    "AlarmCountReset",
    "HHLimit",
    # "HHSeverity",
    "HLimit",
    # "HSeverity",
    "LLimit",
    # "LSeverity",
    "LLLimit",
    # "LLSeverity",
    # "ROCPosLimit",  # rate-of-change, units per second
    # "ROCPosSeverity",
    # "ROCNegSeverity",
    # "ROCNegSeverity",
    # "ROCPeriod",  # period for calculating ROC

    "InAlarm",  # Active when any alarm condition is true
    "AnyInAlarmUnack",  # In alarm and unacknowledged
    "HHInAlarm",  # High high alarm
    "HInAlarm",  # High alarm
    "LLInAlarm",  # Low low alarm
    "LInAlarm",  # Low alarm
    # "ROCPosInAlarm",  # Positive rate-of-change alarm
    # "ROCNegInAlarm",  # Negative rate-of-change alarm

    # "ROC",  # Current calculated rate-of-change

    "HHOperAck",
    "HOperAck",
    "LOperAck",
    "LLOperAck",

    "HHAcked",  # High high ack status
    "HAcked",
    "LLAcked",
    "LAcked",
    # "ROCPosAcked",
    # "ROCNegAcked",
    "HHInAlarmUnack",  # High hith alarm and not acked
    "HInAlarmUnack",
    "LLInAlarmUnack",
    "LInAlarmUnack",
    # "ROCPosInAlarmUnack",
    # "ROCNegInAlarmUnack",

    "Disabled",  # Set when the alarm is disabled

    "HHInAlarmTime",  # Timestamp for High high alarm
    "HHAlarmCount",  # Number of High high alarm activations
    "HInAlarmTime",
    "HAlarmCount",
    "LLInAlarmTime",
    "LLAlarmCount",
    "LInAlarmTime",
    "LAlarmCount",
    # "ROCPosInAlarmTime",
    # "ROCPosAlarmCount",
    # "ROCNegInAlarmTime",
    # "ROCNegAlarmCount",
    "AckTime",  # Timestamp for acknowledgment
    "RetToNormalTime",  # Time when the alarm condition returned to normal
    "AlarmCountResetTime"  # Timestamp for alarm count reset
]
