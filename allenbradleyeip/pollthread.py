import random
from threading import Thread, Event
import time


class PeriodicScanlistPollThread(Thread):

    "A thread that periodically scans a scanlist"

    def __init__(self, logger, scanlist, scan_callback, error_callback):
        super(PeriodicScanlistPollThread, self).__init__()
        self.logger = logger
        self.scanlist = scanlist
        self.scan_callback = scan_callback  # gets called after each polling
        self.error_callback = error_callback

        self.stopped = Event()

        self.failures = 0
        self.backoff = 5  # how much longer to wait per failure
        self.max_backoff = 60

        self._last_debug_report = 0

    def run(self):
        self.logger.info("Starting PeriodicScanlistPollThread %f (%d tags)" %
                         (self.scanlist.period, len(self.scanlist.tags)))
        self.stopped.wait(random.random())  # add some jitter on startup
        while not self.stopped.is_set():
            t0 = time.time()
            try:
                self.scanlist.scan()
                self.scan_callback(self.scanlist)
                dt = time.time() - t0
                self.perhaps_send_debug_report()
                self.failures = 0
                sleeptime = self.scanlist.period - dt
                if sleeptime > 0:
                    self.stopped.wait(sleeptime)
                else:
                    self.logger.warn(
                        ("Scan took longer than sleeptime (%f s)"
                         "in %s %f") %
                        (dt, self.__class__.__name__, self.scanlist.period))
            except Exception as e:
                self.logger.error("Scan failed in %s %f: %r" %
                                  (self.__class__.__name__,
                                   self.scanlist.period, e))
                self.failures += 1
                self.error_callback(self.scanlist.period)
                self.stopped.wait(min(self.max_backoff,
                                      self.failures * self.backoff))

    def perhaps_send_debug_report(self):
        # Something that can be periodically called, just to show that
        # the polling thread is alive and OK.
        now = time.time()
        if self._last_debug_report < (now - 60):
            message = ("Thread for scanlist {0} reporting {1} failures."
                       .format(self.scanlist.period, self.failures))
            if self.failures:
                self.logger.warn(message)
            else:
                self.logger.debug(message)
            self._last_debug_report = now

    def stop(self):
        self.logger.info("Stopping %s %f (%d tags)" %
                         (self.__class__.__name__, self.scanlist.period,
                          len(self.scanlist.tags)))
        self.stopped.set()


class OndemandScanlistPollThread(Thread):

    "A thread that scans a scanlist only if there is demand"

    def __init__(self, logger, scanlist, sleep=0.1):
        super(OndemandScanlistPollThread, self).__init__()

        self.logger = logger
        self.scanlist = scanlist
        self.sleep = sleep
        self.running = False

        self.failures = 0
        self.backoff = 5  # how much to increase wait per consecutive failure
        self.max_backoff = 60

        self._last_debug_report = 0

    def run(self):
        self.running = True
        self.logger.info("Starting OndemandScanlistPollThread %f (%d tags)" %
                         (self.scanlist.period, len(self.scanlist.tags)))
        time.sleep(random.random())
        while self.running:
            time.sleep(self.sleep + min(self.max_backoff, self.backoff * self.failures))
            if self.scanlist.needs_scan:
                try:
                    self.scanlist.scan()
                except Exception as e:  # yes, yes... but we really don't want the thread to die
                    self.logger.error("Scan failed in OndemandScanlistPollThread %f: %s" %
                                      (self.scanlist.period, e))
                    self.failures += 1
                else:
                    self.failures = 0
            self.perhaps_send_debug_report()

    def perhaps_send_debug_report(self):
        now = time.time()
        if self._last_debug_report < (now - 60):
            message = ("Thread for scanlist {0} reporting {1} failures."
                       .format(self.scanlist.period, self.failures))
            if self.failures:
                self.logger.warn(message)
            else:
                self.logger.debug(message)
            self._last_debug_report = now

    def stop(self):
        self.logger.info("Stopping OndemandScanlistPollThread %f (%d tags)" %
                         (self.scanlist.period, len(self.scanlist.tags)))
        self.running = False
