This file contains some thoughts and ideas about the project.

Issues
======

There are some problems with the device that will be hard to solve with the current implementation, since it's built around the C based `etherip` library which handles all PLC communication (see the README for some more details on how it works). The `etherip` library is designed for use with EPICS and some of its "flaws" are probably explained by this.

* Bad connection handling. There's (AFAIK) no way to manually close the socket connection, or re-initialize it. This means safely recovering from error (usually caused by PLC restart or similar) is basically impossible. Restarting the process is perhaps what is usually done in EPICS but in TANGO it's not desirable.

* "Scanlists" are inflexible. The only way to read a tag properly is by adding it to a static list of tags and then read the whole list at once. This is cumbersome, and does not play well with TANGO polling etc (although *maybe* it could work with the new polling implementation in TANGO 9... see below). This also contributes to the next item:

* Bad error handling. E.g. a single unreadable tag can break a whole scanlist.

* No way to list what tags exist in the PLC. The only way to check if a tag exists and is read/writable is to try and because of the previous error, this is a very annoying way to check.

These are the main problems that contribute to the unreliability of the device. I don't think any of them is *urgent*, since the device operates fine under normal circumstances and performance is adequate. But especially the fact that the devices tend to stop working any time there's a PLC software upgrade or network glitch is very annoying, and configuration could be made a lot less painful if the device was able to list tags and verify the configuration by itself.


Solutions
=========

Here are a few possible options on how to make the device better.

Switch to a different communication library
-------------------------------------------

When the device was written (~2014) we weren't able to find any realistic options other than `etherip` or relying on the official integration with `RSLinx` and `OPC` (which always involves windows servers). So a python wrapper for `etherip` was developed (using `Cython`, see https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-etherip). But since then, several other options have appeared and should be evaluated as replacements. I'll list the ones I know about and what my opinion is, but keep in mind that some of them may have progressed since my last check.

* `cpppo` https://github.com/pjkundert/cpppo A very nice, mature pure python library that can read and write tags of all types. It also contains a PLC "simulator" that is very useful for testing! Main drawback: much slower than `etherip`, probably too slow for our purposes :(

* `pycomm` https://github.com/ruscito/pycomm A newer and simpler python library. Can read and write tags (probably not all data types, but that should not be too hard to add if needed). Much faster than `cpppo`, quick tests indicate that it's probably about as fast as `etherip`. Can also list tags on the PLC! Drawbacks: quite new so may not be reliable, development seems a little unsteady (mostly one developer). I think this one looks most promising though!

* `libplctag` https://github.com/kyle-github/libplctag A newer C library that seems promising. There's a python wrapper included. Haven't tried this one but it should be investigated. Main drawback: written in C.

Use TANGO polling
-----------------

The main complication with communication with the PLC is that, in order to reach any kind of "acceptable" performance, the device must not read tags one by one, but instead in "batches", where several tags are read in one operation. This allows the transport layer to pack several requests/responses into a single communication "packet". This is very important since the PLC apparently has very limited resources dedicated to communication. This is also the main reason for the "scanlist" implementation in the `etherip` library. This means that tags need to be grouped together and each group periodically read at some frequency. Writing is less of an issue since it's usually not very frequent anyway. 

The current device uses internal threads for periodic tag reading, but this is not so desirable because it makes the device more complex and it hides the inner workings away from the user. Using TANGO polling would be nice, but with the `etherip` library and the scanlist stuff I could not work out a way to do this that wouldn't be worse than the current implementation. 

Attribute polling in TANGO <9 could not work, since each attribute is read separately even if several have the same period. AFAIK, this has changed in TANGO 9 where all attributes with the same polling period will be read together (using `read_attributes()`), but since `etherip` scanlists are quite static, and the device is not really aware of the TANGO polling configuration, this would be tricky to do in a non-horrible way. Using command polling does not work either because that would require creating one command per polling period (there can be no arguments to a polled command) and this would also be a mess.

If a more flexible PLC library was used, where the list of tags to read could be defined on-the-fly, TANGO 9 attribute polling should work fine. Then we'd just setup TANGO attribute polling for the attributes we want polled, and TANGO would automatically group all attributes with the same period together in a single multi attribute read. The device would perform those as "batch" reads and everyone is happy. The device itself does not have to care at all about scanlists or periodic reading, and all that configuration is done via attribute properties. Even pushing events is all handled by TANGO! The device would only have to care about how to read and write PLC tags, and expose them as attributes. One complication to consider though; TANGO only assigns (maximum) one thread per device, so all "scanlists" would be read by the same thread. This means that if one is polling slowly and takes a long time to complete, it might delay other, faster ones, which would result in error events and stuff. I'm not sure if this would be an issue or not but it's something to consider and test carefully. The PLC communication is always much slower than you expect.
